# DET Hauptprojekt

## Assets:

* [Player Sprites](https://creatorcaz.itch.io/cute-penguin-sprite-idle-run-attack)
* [Isometric Tiles](https://shallow-lagoon.itch.io/mega-isometric-block-pack)
* [Polar Bear](https://opengameart.org/content/galapagos-penguin-and-polar-bear)
* [Tree/Snowman](https://jonathancurtis.itch.io/snowy-tileset)

## Meilensteine: 

* [Google Doc](https://docs.google.com/document/d/1Iz4PYafPEEZifM4fkjdpAKbF8TNi6SSTQUjO_ochvKI/edit?usp=sharing)
* [Google Presentation](https://docs.google.com/presentation/d/17jyyKFI6qqPcYzcNhFvO5coaoKJ6RBwIa0KsZmTyjho/edit?usp=sharing)
* [Gantt Diagramm](https://lucid.app/lucidchart/invitations/accept/99ea4a85-285a-4169-aeea-9deb4efc19b7)
