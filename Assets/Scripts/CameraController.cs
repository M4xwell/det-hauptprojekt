using UnityEngine;

namespace DefaultNamespace
{
    public class CameraController : MonoBehaviour
    {
        private Transform playerPos;

        public GameObject player;

        private void Awake()
        {
            playerPos = player.transform;
        }

        private void Update()
        {
            transform.position = new Vector3(playerPos.position.x, playerPos.position.y, transform.position.z);
        }
    }
}