using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace DefaultNamespace
{
    public class CellularAutomata
    {
        public int width;
        public int height;

        [Range(40, 60)] public float randomFillPercent;

        private int[,] map;

        public CellularAutomata(int width, int height, float randomFillPercent)
        {
            this.width = width;
            this.height = height;
            this.randomFillPercent = randomFillPercent;
        }

        public int[,] GenerateMap()
        {
            map = RandomFillMap(new int[width, height]);

            for (var i = 0; i < 5; i++) map = SmoothMap(map);

            return map;
        }

        public int[,] RandomFillMap(int[,] m)
        {
            var pseudoRandom = new Random();

            for (var x = 0; x < width; x++)
            for (var y = 0; y < height; y++)
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    m[x, y] = 1;
                else
                    m[x, y] = pseudoRandom.Next(0, 100) < randomFillPercent ? 1 : 0;
            return m;
        }

        public int[,] SmoothMap(int[,] m)
        {
            for (var x = 0; x < width; x++)
            for (var y = 0; y < height; y++)
            {
                var neighbourWallTiles = GetSurroundingGroundCount(x, y, m);

                if (neighbourWallTiles > 4)
                    m[x, y] = 1;
                else if (neighbourWallTiles < 4)
                    m[x, y] = 0;
            }

            return m;
        }

        private int GetSurroundingGroundCount(int gridX, int gridY, int[,] m)
        {
            var wallCount = 0;
            for (var neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
            for (var neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
                if (neighbourX >= 0 && neighbourX < width && neighbourY >= 0 && neighbourY < height)
                {
                    if (neighbourX != gridX || neighbourY != gridY) wallCount += m[neighbourX, neighbourY];
                }
                else
                {
                    wallCount++;
                }

            return wallCount;
        }
    }
}