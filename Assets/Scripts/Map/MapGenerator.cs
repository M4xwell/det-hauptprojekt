using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = System.Random;

namespace DefaultNamespace
{
    public class MapGenerator
    {
        public Tilemap GroundTilemap;
        public Tilemap WaterTileMap;

        public TileBase snowTile;
        public TileBase snowGrassTile;
        public TileBase waterTile;
        public TileBase waterTileHalf;

        private int SIZE = 64;

        private CellularAutomata Cellular;

        public MapGenerator(Tilemap groundTilemap, Tilemap waterTilemap, TileBase snowTile, TileBase snowGrassTile, TileBase waterTile,
            TileBase waterTileHalf)
        {
            GroundTilemap = groundTilemap;
            WaterTileMap = waterTilemap;
            this.snowTile = snowTile;
            this.snowGrassTile = snowGrassTile;
            this.waterTile = waterTile;
            this.waterTileHalf = waterTileHalf;
        }

        public int[,] Generate()
        {
            Cellular = new CellularAutomata(SIZE, SIZE, 45f);
            var map = Cellular.GenerateMap();
            PopulateLevel(map);

            return map;
        }

        private void PopulateLevel(int[,] map)
        {
            var random = new Random(1);
            for (var x = 0; x < SIZE; x++)
            for (var y = 0; y < SIZE; y++)
                if (x == 0 || x == 64 - 1 || y == 0 || y == 64 - 1)
                {
                    WaterTileMap.SetTile(new Vector3Int(x, y, 0), waterTile);
                }
                else if (map[x, y] == 1)
                {
                    var position = new Vector3Int(x, y, 0);
                    if (GroundTilemap.GetTile(position) == null)
                        WaterTileMap.SetTile(new Vector3Int(x, y, 0), waterTileHalf);
                }
                else if (map[x, y] == 0)
                {
                    var position = new Vector3Int(x, y, 0);
                    if (random.Next(10) == 1)
                    {
                        if (GroundTilemap.GetTile(position) == null) GroundTilemap.SetTile(position, snowTile);
                    }
                    else
                    {
                        if (GroundTilemap.GetTile(position) == null) GroundTilemap.SetTile(position, snowGrassTile);
                    }
                }
        }
    }
}