namespace DefaultNamespace
{
    public interface Damagable
    {
        void TakeDamage(int damage);
    }
}