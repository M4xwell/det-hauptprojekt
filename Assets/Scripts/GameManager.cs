﻿using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject Tree;
    public GameObject Player;

    public Texture2D cursor;

    public Tilemap GroundTileMap;
    public Tilemap WaterTileMap;

    public TileBase SnowTile;
    public TileBase WaterTile;
    public TileBase WaterTileHalf;
    public TileBase snowGrassTile;

    private List<Vector2> SpawnPoints;

    public GameObject[] Presents;

    public int currentLevel = 1;

    private Vector3 playerPosition;

    private void Awake()
    {
        playerPosition = Player.transform.position;

        MakeSingleton();
        CreateLevel();
    }

    private void FixedUpdate()
    {
        var treeController = Tree.GetComponent<TreeController>();
        switch (currentLevel)
        {
            case 1:
                if (treeController.currentGifts == 2)
                {
                    CreateLevel();
                    currentLevel = 2;
                }

                break;
            case 2:
                if (treeController.currentGifts == 4)
                {
                    CreateLevel();
                    currentLevel = 3;
                }

                break;
            case 3:
                if (treeController.currentGifts == 8)
                {
                    CreateLevel();
                    currentLevel = 4;
                }

                break;
            case 4: // game won
                SceneManager.LoadScene("EndGame");
                break;
        }
    }

    private void CreateLevel()
    {
        var mapGenerator = new MapGenerator(GroundTileMap, WaterTileMap, snowGrassTile, SnowTile, WaterTile, WaterTileHalf);
        mapGenerator.Generate();

        Player.transform.position = playerPosition;

        setGifts();
    }

    private void setGifts()
    {
        if (currentLevel == 1)
        {
            Presents[0].SetActive(true);
            Presents[1].SetActive(true);
        }

        if (currentLevel == 2)
        {
            Presents[0].SetActive(true);
            Presents[1].SetActive(true);
            Presents[2].SetActive(true);
            Presents[3].SetActive(true);
        }

        if (currentLevel == 3)
        {
            Presents[0].SetActive(true);
            Presents[1].SetActive(true);
            Presents[2].SetActive(true);
            Presents[3].SetActive(true);
            Presents[4].SetActive(true);
            Presents[5].SetActive(true);
            Presents[6].SetActive(true);
            Presents[7].SetActive(true);
        }
    }

    public void LoadNextLevel()
    {
        // Go to level 2
        if (currentLevel == 1)
        {
            currentLevel = 2;
            SceneManager.LoadScene("Game");
        }

        // Go to level 3
        if (currentLevel == 2)
        {
            currentLevel = 3;
            SceneManager.LoadScene("Game");
        }

        // Game finished
        if (currentLevel == 3)
        {
        }
    }

    private void Start()
    {
        SetCursor();
    }

    private void OnMouseEnter()
    {
        SetCursor();
    }

    private void OnMouseExit()
    {
        SetCursor();
    }

    private void SetCursor()
    {
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
    }

    private void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }
}