﻿using System;
using Enemy;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private void Update()
    {
        Destroy(gameObject, 5.0f);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("COLLIDE: " + other);

            var component = other.gameObject.GetComponent<EnemyStatus>();
            component.TakeDamage(1);

            Destroy(gameObject);
        }
    }
}