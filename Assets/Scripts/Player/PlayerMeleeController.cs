using System;
using Enemy;
using UnityEngine;

namespace Player
{
    public class PlayerMeleeController : MonoBehaviour
    {
        public Animator Animator;


        private GameObject enemy;

        private void Update()
        {
            if (Input.GetAxisRaw("Jump") > 0)
                Animator.SetBool("Attack", true);
            
            if (enemy != null)
            {
                var component = enemy.GetComponent<EnemyStatus>();
                component.TakeDamage(1);
            }
            else
                Animator.SetBool("Attack", false);
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Enemy")) enemy = other.gameObject;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Enemy")) enemy = null;
        }
    }
}