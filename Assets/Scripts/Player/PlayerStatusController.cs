using System;
using DefaultNamespace;
using UnityEngine;

namespace Player
{
    public class PlayerStatusController : MonoBehaviour, Damagable
    {
        public int Health = 3;

        private void FixedUpdate()
        {
            if (Health <= 0)
                Debug.Log("DEAD");
            //Destroy(gameObject);
        }


        public void TakeDamage(int damage)
        {
            Health -= damage;
        }
    }
}