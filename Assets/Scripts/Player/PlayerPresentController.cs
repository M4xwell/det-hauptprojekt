﻿using System;
using UnityEngine;

namespace Player
{
    public class PlayerPresentController : MonoBehaviour
    {
        public int Counter;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Present"))
            {
                Counter++;
                other.gameObject.SetActive(false);
            }

            if (other.gameObject.CompareTag("Tree") && Counter > 0)
            {
                var treeController = other.gameObject.GetComponent<TreeController>();
                treeController.currentGifts += Counter;
                Counter = 0;
            }
        }

        private void OnTriggerStay(Collider other)
        {
            throw new NotImplementedException();
        }
    }
}