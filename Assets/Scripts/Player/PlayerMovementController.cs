using System;
using UnityEngine;

namespace Player
{
    public class PlayerMovementController : MonoBehaviour
    {
        public float moveSpeed = 2f;
        public float waterSpeed = 4f;

        public Animator Animator;

        private Rigidbody2D _rb;
        public Rigidbody2D FirePointRb;

        private Vector2 _movement;

        private bool _isWater;

        private void Start()
        {
            _rb = gameObject.GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            _movement.x = Input.GetAxisRaw("Horizontal");
            _movement.y = Input.GetAxisRaw("Vertical");

            Animate();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Water")) _isWater = true;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Water")) _isWater = false;
        }

        private void FixedUpdate()
        {
            if (!_isWater)
                _rb.MovePosition(_rb.position + _movement * (moveSpeed * Time.fixedDeltaTime));
            else
                _rb.MovePosition(_rb.position + _movement * (waterSpeed * Time.fixedDeltaTime));

            FirePointRb.MovePosition(_rb.position + _movement * (moveSpeed * Time.fixedDeltaTime));
        }

        private void Animate()
        {
            if (_movement.x > 0)
            {
                Animator.SetBool("Right", true);
                Animator.SetBool("Left", false);
                Animator.SetInteger("Horizontal", 1);
            }
            else if (_movement.x < 0)
            {
                Animator.SetBool("Right", false);
                Animator.SetBool("Left", true);
                Animator.SetInteger("Horizontal", -1);
            }
            else
            {
                Animator.SetInteger("Horizontal", 0);
            }
        }
    }
}