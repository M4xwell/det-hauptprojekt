﻿using UnityEngine;

public class PlayerShootingController : MonoBehaviour
{
    public GameObject SnowBall;

    public GameObject FirePoint;

    public float Force = 5f;

    public Animator Animator;


    private void FixedUpdate()
    {
        if (Input.GetButtonDown("Fire1") && !Animator.GetBool("Attack"))
        {
            var snowBall = Instantiate(SnowBall, FirePoint.transform.position, Quaternion.identity);
            var rb = FirePoint.GetComponent<Rigidbody2D>();

            var angle = RotateFirePoint();
            rb.rotation = angle;

            snowBall.GetComponent<Rigidbody2D>().AddForce(FirePoint.transform.up * Force, ForceMode2D.Impulse);
        }
    }

    private float RotateFirePoint()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        var rb = FirePoint.GetComponent<Rigidbody2D>();

        var lookDir = mousePos - rb.position;

        return Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
    }
}