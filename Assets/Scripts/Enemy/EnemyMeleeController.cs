﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Player;
using UnityEngine;

public class EnemyMeleeController : MonoBehaviour
{
    public Animator Animator;

    public CircleCollider2D LeftCollider;

    public CircleCollider2D RightCollider;

    public int Damage = 20;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Animator.SetBool("Attack", true);

            var component = other.gameObject.GetComponent<PlayerStatusController>();
            StartCoroutine(takeDamage(component));
        }
    }

    IEnumerator takeDamage(PlayerStatusController component)
    {
        component.TakeDamage(1);
        yield return new WaitForSeconds(2);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player")) Animator.SetBool("Attack", false);
    }
}