﻿using System;
using System.Collections;
using System.Collections.Generic;
using Enemy;
using Pathfinding;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject EnemyPrefab;

    public GameObject[] Path;

    private GameObject enemy;

    private int pathIndex;

    private void Start()
    {
        pathIndex = UnityEngine.Random.Range(0, 6);

        enemy = Instantiate(EnemyPrefab, Path[pathIndex].transform.position, Quaternion.identity);
    }

    private void Update()
    {
        if (enemy != null)
        {
            var b = enemy.GetComponent<EnemyMovementController>().FollowingPlayer;
            
            if (!b)
            {
                move();
            }
        }
    }

    private void move()
    {
        if (pathIndex <= Path.Length - 1)
        {
            enemy.transform.position = enemy.GetComponent<EnemyMovementController>()
                .Move(enemy.transform.position, Path[pathIndex].transform.position);

            if (enemy.transform.position == Path[pathIndex].transform.position) pathIndex++;
        }

        if (pathIndex == Path.Length) pathIndex = 0;
    }
}