﻿using DefaultNamespace;
using UnityEngine;

namespace Enemy
{
    public class EnemyStatus : MonoBehaviour, Damagable
    {
        public int Health = 3;

        public void TakeDamage(int damage)
        {
            if (Health <= 0) Destroy(gameObject);

            Health -= damage;


            if (Health <= 0) Destroy(gameObject);
        }
    }
}