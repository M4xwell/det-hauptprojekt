﻿using System;
using System.Collections;
using System.Collections.Generic;
using Enemy;
using Pathfinding;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    private Transform target;

    private float speed = 200f;

    public float nextWaypointDistance = 3f;

    public Transform enemyGFX;

    private Path path;
    private int currentWayPoint = 0;
    private bool reachedEndOfPath = false;

    private Seeker seeker;
    private Rigidbody2D rb;

    private EnemyMovementController _movementController;

    private void Start()
    {
        target = GameObject.Find("Player").transform;

        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        InvokeRepeating("UpdatePath", 0f, 0.5f);

        _movementController = gameObject.GetComponent<EnemyMovementController>();
    }

    private void UpdatePath()
    {
        if (seeker.IsDone())
        {
            seeker.StartPath(rb.position, target.position, OnPathComplete);
        }
    }

    private void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWayPoint = 0;
        }
    }

    private void FixedUpdate()
    {
        if (_movementController.FollowingPlayer)
        {
            if (path == null) return;

            if (currentWayPoint >= path.vectorPath.Count)
            {
                reachedEndOfPath = true;
                return;
            }

            reachedEndOfPath = false;

            var direction = ((Vector2) path.vectorPath[currentWayPoint] - rb.position).normalized;
            var force = direction * (speed * Time.fixedDeltaTime);

            rb.AddForce(force);

            var distance = Vector2.Distance(rb.position, path.vectorPath[currentWayPoint]);

            if (distance < nextWaypointDistance) currentWayPoint++;

            if (rb.velocity.x >= 0.01f && force.x > 0f)
                enemyGFX.localScale = new Vector3(-1f, 1f, 1f);
            else if (rb.velocity.x <= -0.01 && force.x < 0f) enemyGFX.localScale = new Vector3(1f, 1f, 1f);
        }
    }
}