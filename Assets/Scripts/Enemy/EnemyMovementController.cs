using System;
using UnityEngine;

namespace Enemy
{
    public class EnemyMovementController : MonoBehaviour
    {
        public float moveSpeed = 2f;
        public float waterSpeed = 1f;

        public Animator Animator;

        private GameObject Player;

        public bool FollowingPlayer;

        private bool _isWater;

        private void Start()
        {
            Player = GameObject.Find("Player");
        }

        private void FixedUpdate()
        {
            float distance = Vector3.Distance(gameObject.transform.position, Player.transform.position);

            if (distance < 4f)
            {
                FollowingPlayer = true;
            }
            else
            {
                FollowingPlayer = false;
            }
        }

        public Vector2 Move(Vector2 current, Vector2 target)
        {
            if (current.x - target.x < 0)
            {
                Animator.SetBool("Right", true);
                Animator.SetBool("Left", false);
                Animator.SetInteger("Horizontal", 1);
            }
            else
            {
                Animator.SetBool("Right", false);
                Animator.SetBool("Left", true);
                Animator.SetInteger("Horizontal", -1);
            }

            if (!_isWater)
                return Vector2.MoveTowards(current, target, moveSpeed * Time.deltaTime);

            return Vector2.MoveTowards(current, target, waterSpeed * Time.deltaTime);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Water")) _isWater = true;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Water")) _isWater = false;
        }
    }
}