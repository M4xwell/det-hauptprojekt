using System;
using UnityEngine;
using UnityEngine.UI;

namespace menu
{
    public class LevelMenuController : MonoBehaviour
    {
        public Text currentLevel;
        public GameManager gm;

        private void FixedUpdate()
        {
            currentLevel.text = gm.currentLevel + "";
        }
    }
}