using UnityEngine;
using UnityEngine.SceneManagement;

namespace menu
{
    public class MenuController : MonoBehaviour
    {
        public void StartGame()
        {
            SceneManager.LoadScene("Game");
        }

        public void ShowLevelGeneration()
        {
            SceneManager.LoadScene("LevelGeneration");
        }
    }
}