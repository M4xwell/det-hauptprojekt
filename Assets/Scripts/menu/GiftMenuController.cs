﻿using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.UI;

public class GiftMenuController : MonoBehaviour
{
    public GameObject Player;

    public Text currentGifts;
    public Text maxGifts;

    public GameManager gm;

    private void Start()
    {
        setMaxGifts();
    }

    private void FixedUpdate()
    {
        setMaxGifts();

        currentGifts.text = Player.GetComponent<PlayerPresentController>().Counter + "";
    }

    private void setMaxGifts()
    {
        switch (gm.currentLevel)
        {
            case 1:
                maxGifts.text = 2 + "";
                break;
            case 2:
                maxGifts.text = 4 + "";
                break;
            case 3:
                maxGifts.text = 8 + "";
                break;
        }
    }
}