﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

namespace menu
{
    public class HUDLevelgenerationController : MonoBehaviour
    {
        private List<int[,]> mapList;
        public List<Tilemap> GridList;

        public Grid grid;

        public GameObject TileMapPrefab;

        public TileBase SnowTile;
        public TileBase WaterTile;

        private int currentIndex;

        private int SIZE = 64;

        private void Start()
        {
            currentIndex = 0;

            var controller = new ShowLevelGenerationController();
            mapList = controller.GridList;

            GridList = generateGrids();
            GridList[0].gameObject.SetActive(true);
        }

        private List<Tilemap> generateGrids()
        {
            var list = new List<Tilemap>();
            foreach (int[,] map in mapList)
            {
                var instantiate = Instantiate(TileMapPrefab);

                instantiate.transform.parent = grid.gameObject.transform;

                instantiate.SetActive(false);
                var tilemap = instantiate.GetComponent<Tilemap>();
                PopulateLevel(map, tilemap);
                list.Add(tilemap);
                Destroy(TileMapPrefab);
            }

            return list;
        }

        public void Next()
        {
            if (currentIndex < mapList.Count - 1)
            {
                foreach (Tilemap tilemap in GridList)
                {
                    tilemap.gameObject.SetActive(false);
                }

                currentIndex++;
                GridList[currentIndex].gameObject.SetActive(true);
            }
        }

        public void Back()
        {
            if (currentIndex > 0)
            {
                foreach (Tilemap tilemap in GridList)
                {
                    tilemap.gameObject.SetActive(false);
                }

                currentIndex--;
                GridList[currentIndex].gameObject.SetActive(true);
            }
        }

        public void BackToMenu()
        {
            SceneManager.LoadScene("MainMenuScene");
        }
        
        public void Replay()
        {
            SceneManager.LoadScene("LevelGeneration");
        }

        private void PopulateLevel(int[,] map, Tilemap tilemap)
        {
            for (var x = 0; x < SIZE; x++)
            {
                for (var y = 0; y < SIZE; y++)
                {
                    var position = new Vector3Int(x, y, 0);

                    if (map[x, y] == 0)
                    {
                        tilemap.SetTile(position, SnowTile);
                    }
                    else
                    {
                        tilemap.SetTile(position, WaterTile);
                    }
                }
            }
        }
    }
}