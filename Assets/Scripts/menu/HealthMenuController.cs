﻿using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.UI;

public class HealthMenuController : MonoBehaviour
{
    public GameObject Player;

    public GameObject Heart1;
    public GameObject Heart2;
    public GameObject Heart3;

    private void FixedUpdate()
    {
        switch (Player.GetComponent<PlayerStatusController>().Health)
        {
            case 1:
                Heart1.SetActive(true);
                break;
            case 2:
                Heart1.SetActive(true);
                Heart2.SetActive(true);
                break;
            case 3:
                Heart1.SetActive(true);
                Heart2.SetActive(true);
                Heart3.SetActive(true);
                break;
        }
    }
}