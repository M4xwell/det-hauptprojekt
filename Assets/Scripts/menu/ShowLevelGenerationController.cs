﻿using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using UnityEngine;

namespace menu
{
    public class ShowLevelGenerationController : MonoBehaviour
    {
        public List<int[,]> GridList;

        private const int SIZE = 64;

        public ShowLevelGenerationController()
        {
            GridList = new List<int[,]>();
            var cellularAutomata = new CellularAutomata(SIZE, SIZE, 45f);
            
            int[,] map = cellularAutomata.RandomFillMap(new int[SIZE, SIZE]);
            GridList.Add(map);
            
            var ints = (int[,]) map.Clone();
            GridList.Add(cellularAutomata.SmoothMap(ints));
            
            ints = (int[,]) ints.Clone();
            GridList.Add(cellularAutomata.SmoothMap(ints));
            
            ints = (int[,]) ints.Clone();
            GridList.Add(cellularAutomata.SmoothMap(ints));
            
            ints = (int[,]) ints.Clone();
            GridList.Add(cellularAutomata.SmoothMap(ints));
            
            ints = (int[,]) ints.Clone();
            GridList.Add(cellularAutomata.SmoothMap(ints));

        }
    }
}